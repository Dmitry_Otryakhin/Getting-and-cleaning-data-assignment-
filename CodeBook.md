# CodeBook for Tidy_data_set.txt

### What is the Tidy_data_set.txt
It seems not the right thing to do to open file with Excel or text editor- it will not be opened correctly. The better way is to load it into R with read.table(). Since you have loaded data into R you can see that first 79 columns are means and stds of the processed original data sets. Columns **80- 82 are respectively subject (subjects number), activity (activity's number) and activity name**. 

### How tidy data set appears

My script consist of 5 functions each of them perform different steps of computation. In the begining script joins test table with it's subject and activity labels and do the same thing with train table. Then, script joins two obtained table with each other. In the second part of computations function loads feature's names from the features.txt and put them into variable names. After that script changes names into descriptive form as required in p.3 of assignment (BodyBody to Body, t to time and etc.) Separetely, function creates names for variables responsible for subject names and activity labels. Third function takes table generated before and extracts only measurements on the mean and standart deviation for each measurement, subjects, and activity labels. Forth function creates column with descriptive names of activities. And the last thing that programm do is creating tidy data set with the average of each variable for each activity and each subject.



