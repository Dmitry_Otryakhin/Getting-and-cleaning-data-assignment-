# Readme for the repository

### What files are in the repo
Here you can see 4 files- readme.md, Tidy_data_set.txt, CodeBook.md and run_analysis.R. Tidy_data_set is a set that my R code, run_analysis, have created. run_analysis.R- is an R file contains script code. run_analysis.R- is the only script perfom computations and consists of five functions (see details below). 

### Technical details
I have Windows 7 Home Basic, R version 3.1.1, R studio version 0.98.1049

### Requirements
For starting computation you shouldn't have specific packages installed. Script uses only functions from the basic package.
One thing that you should know that script loads all files from the UCI HAR Dataset directory, that is IN the working directory. Please do not make any changes in UCI HAR Dataset directory before and in the middle of running script, because it can affect the result of computation.

### Internal structure of script
There are 5 internal functions in the script:

1. Download_n_merg
2. Names_Maker
3. mean_n_stds
4. Name_activity
5. Tidy_data_set

(1) script downloads data sets and join both- activity an subject labels and then generate table with all merged data.

(2) add names to the final data set from features.txt and names columns with subject and activity labels. After that second function format names of variables to make them descriptive by using gsub (as much as it possible for sense of given raw data).

(3) This function extracts only means and standart deviations from data frame created in p. 1, 2 and join them to subjects and activities.

(4) Creates column whith names of each activity- label 1 match with "Walking", 2- with "Walking upstairs" and so forth.

(5) Creates required tidy data set

### Description of tidy data set
See in CodeBook.md
